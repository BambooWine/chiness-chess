﻿#include "chesspiece.h"

ChessPiece::ChessPiece() {
}

ChessPiece::ChessPiece(const ChessPiece &r) {
    this-> _type = r._type;
    this-> _row = r._row;
    this-> _col = r._col;
    this-> _id = r._id;
    this-> _isRed = r._isRed;
    this-> _isDead = r._isDead;
}

ChessPiece::~ChessPiece() {
}

void ChessPiece::init(int id) {
    _id = id;
    _isDead = false;
    _isRed = (id >= 16);
    struct {
        int row, col;
        ChessPiece::TYPE type;
    } pos[16] = {
        {0, 0, ChessPiece::CHE},
        {0, 1, ChessPiece::MA},
        {0, 2, ChessPiece::XIANG},
        {0, 3, ChessPiece::SHI},
        {0, 4, ChessPiece::JIANG},
        {0, 5, ChessPiece::SHI},
        {0, 6, ChessPiece::XIANG},
        {0, 7, ChessPiece::MA},
        {0, 8, ChessPiece::CHE},
        {2, 1, ChessPiece::PAO},
        {2, 7, ChessPiece::PAO},
        {3, 0, ChessPiece::BING},
        {3, 2, ChessPiece::BING},
        {3, 4, ChessPiece::BING},
        {3, 6, ChessPiece::BING},
        {3, 8, ChessPiece::BING},
    };
    if(id < 16) {
        _row = pos[id].row;
        _col = pos[id].col;
        _type = pos[id].type;
    } else {
        _row = 9 - pos[id - 16].row;
        _col = 8 - pos[id - 16].col;
        _type = pos[id - 16].type;
    }
}

QString ChessPiece::getText() {
    switch (_type) {
        case JIANG:
            return QString::fromLocal8Bit("将");
        case CHE:
            return QString::fromLocal8Bit("车");
        case MA:
            return QString::fromLocal8Bit("马");
        case XIANG:
            return QString::fromLocal8Bit("象");
        case SHI:
            return QString::fromLocal8Bit("仕");
        case PAO:
            return QString::fromLocal8Bit("炮");
        case BING:
            return QString::fromLocal8Bit("兵");
        default:
            break;
    }
    return "";
}
