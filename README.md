
# ChineseChess

## 1. 介绍

前段时间在网上看到有人在用qt做象棋，心血来潮准备自己也实现一下，先看一波效果图（项目存在一些瑕疵）：

![效果图](https://gitee.com/BambooWine/MyPhotos/raw/master/img/23425)

## 2. 使用说明

+ app文件夹

  已经发布好的程序，点击exe文件即可运行

+ src文件夹

  （本人环境：win10 + Qt5.99 + MSVC2017）

  源目录文件，使用qt打开 ChineseChess.pro文件，构建运行即可（可能存在一些版本差异，环境问题）


## 3. 类关系图

![类图](https://gitee.com/BambooWine/MyPhotos/raw/master/img/23439)

本程序较为简单，设计上分为四个没有继承关系的类：

+ Widget：界面类；负责界面重绘，鼠标点击和按钮点击事件响应，并创建棋子,控制类,计算类等
+ ChessPiece：棋子类；包含棋子的属性：id, 所在行列, 是否是红方阵营, 是否死亡
+ Control：控制类；包括棋子走棋方法：是否可以走棋, 保存走棋路径等等
+ Computer：电脑计算类；运用最大最小值算法求得最优走棋


![类包含图](https://gitee.com/BambooWine/MyPhotos/raw/master/img/23442)


## 4. 用到的技术手段

### 4.1 界面计算分离

由于博弈树会很大，如果将所有的计算都放在widget类中计算会出现卡界面问题；所以通过类的设计，分为widget和computer两个类，通过棋子这个中间变量来进行通信

### 4.2 线程

使用多线程来实现界面计算分离，使得计算最优走棋时不会出现卡界面问题

### 4.3 定时器

定义定时器，timeout连接槽函数，更新各方等待时间


## 5. Qt开发象棋过程问题记录

### 5.1 问题 1. 在初次开发中存在内存泄漏问题
> 问题描述：在初次开发中，出现运行后电脑卡机现象

<font size = 4>**解决：**</font> 一般出现运行程序电脑卡机现象，是大量内存泄漏导致的；回顾程序，大量使用堆内存的地方就是博弈树vector保存路径；查看后发现在使用该vector时，只是removeLast，并未真正释放申请的堆内存，然后delete，解决。


### 5.2 问题 2. 类设计糟糕，使得计算和界面未分离
> 问题描述：由于时间关系，在类的设计上显得糟糕，计算机计算类直接继承于界面类，导致计算量大时，存在卡界面问题

<font size = 4>**解决：**</font> 花时间将项目重写，对项目进行功能分割，使得项目分为：界面类，控制类，棋子类，电脑计算类；从而使得计算和界面分离


### 5.3 问题 3. 重写项目后，效率降低了十倍
> 问题描述：重写项目后，发现计算效率差了十倍，花了两天业余时间，通过不断缩小范围qDebug，还是找不到问题

<font size = 4>**解决：**</font> 无奈就又开始重写，最后发现是因为原来项目中使用原始数组来存储棋子，但是新的项目使用QVector存储棋子，原因就在这里，不清楚为什么使用QVector的效率这么低，最后还是换成了数组存储。


### 5.4 问题 4. 信号和槽连接问题

> 问题描述：自定义类和继承自QWidget的类使用信号和槽通信，发现连接不上

```
QObject::connect: No such slot QObject::startComputerSlot() in ..\ChineseChess\widget.cpp:34
QObject::connect:  (sender name:   'Widget')
QObject::connect: No such signal QObject::sendComputerBestStep(Step *) in ..\ChineseChess\widget.cpp:35
QObject::connect:  (receiver name: 'Widget')
```

<font size = 4>**解决：**</font> 这个问题以前有遇到过； 在自定义的类中，使用信号和槽，需要继承自QObject类，并且在类头添加 `Q_OBJECT`宏


### 5.5 问题 5. 使用线程之后，界面频繁重绘
> 问题描述：将项目重写一遍后，做好了界面和计算分离，可以添加线程使得计算时不会卡界面；但是在添加线程后会出现界面频繁重绘问题（ 现象就是棋子会频繁跳动）

<font size = 4>**解决：**</font> 虽然还不清楚为什么添加线程后会出现这样的问题，但是可以知道线程和界面唯一的联系就是棋子变量，所以只需要在线程中做一份棋子变量的拷贝，让其在线程计算中使用即可