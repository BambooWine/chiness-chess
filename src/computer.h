﻿#ifndef COMPUTER_H
#define COMPUTER_H

#include <QMap>
#include <QObject>
#include <QVector>
#include "chesspiece.h"
#include "global.h"

class Control;

class Computer: public QObject {
    Q_OBJECT
public:
    Computer(ChessPiece r[32], bool &isRed);
    ~Computer();
public:
    Step *getBestMove();                                        // 获取最优的一步
public slots:
    void startComputerSlot();
signals:
    void sendComputerBestStep(Step *);                          // 通过信号和槽发送电脑最优走棋
private:
    void getAllPossibleMove(QVector<Step *> &steps);            // 获取所有可以走的路径
    void fakeMove(Step *&step);                                 // 假使移动一步
    void refakeMove(Step *&step);                               // 将假使移动的棋子放回原处
    int calcScore();                                            // 计算当前局势得分
    int getMinScore(qint8 level, int curMaxScore);              // 最大最小值算法:获取最小值
    int getMaxScore(qint8 level, int curMinScore);              // 最大最小值算法:获取最大值
private:
    void init();
private:
    QMap<ChessPiece::TYPE, int> _scoreTable;                    // 定义每个棋子的价值
    qint8 _level;                                               // 最大最小值算法的层次
    qint8 _boradTable[10][9];                                   // 定义棋盘坐标
    ChessPiece *_original_s;                                    // 当前所有棋子的引用
    ChessPiece *_s;                                             // 当前所有棋子引用的拷贝
    Control *_pCtrl;                                            // 棋子控制类
    bool &_isRedTurn;                                           // 当前是否是红色一方
};

#endif // COMPUTER_H
