﻿#ifndef CHESSPIECE_H
#define CHESSPIECE_H

#include <QString>

class ChessPiece {
public:
    ChessPiece();
    ChessPiece(const ChessPiece &);
    ~ChessPiece();

public:
    void init(int id);          // 棋子初始化
    QString getText();          // 获取棋子文字

public:
    // 棋子类型
    enum TYPE {
        JIANG, CHE, MA, XIANG, SHI, PAO, BING
    };
    static const int r = 20;    // 棋子半径
    TYPE _type;                 // 棋子类型
    qint8 _row;                 // 所在行
    qint8 _col;                 // 所在列
    qint8 _id;                  // 棋子id
    bool _isRed;                // 红方or黑方
    bool _isDead;               // 是否存活
};

#endif // CHESSPIECE_H
