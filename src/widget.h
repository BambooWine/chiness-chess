﻿#ifndef WIDGET_H
#define WIDGET_H

#ifdef WIN32
    #pragma execution_character_set("utf-8")
#endif

#include <QTime>
#include <QTimer>
#include <QWidget>
#include <QVector>
#include <QThread>
#include <QPainter>
#include <QLCDNumber>
#include <QCoreApplication>
#include "global.h"
#include "chesspiece.h"

class Control;
class Computer;

QT_BEGIN_NAMESPACE
namespace Ui {
    class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget {
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

public:
    void drawChess(QPainter &painter, qint8  id);               // 画棋子
    QPoint getCenter(qint8  id);                                // 获取中心点
    bool getRowCol(QPoint pt, int &row, int &col);              // 获取点击时鼠标坐标对应的行和列
    void personMove(qint8 id, qint8 row, qint8 col);            // 人工走棋

signals:
    void startComputerSignal();
private:
    void init();                                // 初始化
    void regretSingleStep(short stepNums);      // 悔棋一步
    void reverseNowLabel();                     // 修改当前NowLabel的内容
    bool judgeRedState();                       // 判断红色一方状态
    bool judgeBlackState();                     // 判断黑色一方状态
    void initRedTimerAndLcd();                  // 重置红色方定时器和LcdNumber
    void initBlackTimerAndLcd();                // 重置黑色方定时器和LcdNumber

public slots:
    void on_saveStepsBtn_clicked();             // 保存棋谱
    void on_firstStepBtn_clicked();             // 我方先手
    void on_regretBtn_clicked();                // 悔棋
    void recvComputerBestStep(Step *);          // 通过信号和槽获得电脑最优走棋
    void redTimerSlot();                        // 红方定时器溢出槽函数
    void blackTimerSlot();                      // 黑方定时器溢出槽函数

private:
    Ui::Widget *ui;
    QPainter _painter;
    Control *_pCtrl;                            // 棋子控制类
    Computer *_pCom;                            // 电脑走棋类
    QVector<Step *> theAllSteps;                // 记录棋谱
    QThread _thread;                            // 线程类, 用于电脑走棋, 使其与界面分离
    QTimer redTimer;                            // 红色方定时器
    QTimer blackTimer;                          // 黑色方定时器

private:
    ChessPiece *_pLastMoveChess;                // 最近一次移动的棋子
    ChessPiece _s[32];                          // 定义32个棋子
    qint8 _selectId;                            // 当前选中棋子Id
    bool _isRedTurn;                            // 当前是否为红方走棋, 用于轮流走棋
};
#endif // WIDGET_H
