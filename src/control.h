﻿#ifndef CONTROL_H
#define CONTROL_H

#include <QMap>
#include <QVector>
#include "global.h"
#include "chesspiece.h"

class ChessPiece;

class Control {
public:
    Control() = delete;
    explicit Control(bool &isRed);
public:
    bool canMove(ChessPiece *_s, bool isPerson, qint8 &id, qint8 destRow, qint8 destCol, qint8 killId);
    bool canMove(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol, qint8 killId);
    qint8 isExistChess(ChessPiece *_s, qint8 row, qint8 col);                                       // 检测指定行和列是否有棋子,有返回id,没有返回-1
    void moveChess(ChessPiece *_s, const Step *const step);                                         // 移动棋子
    void moveChess(ChessPiece *_s, qint8 moveId, qint8 killId, qint8 rowTo, qint8 colTo);           // 移动棋子
    void reSaveChess(ChessPiece *_s, int id);                                                       // 将被吃的棋子救活
    Step *saveStep(ChessPiece *_s, qint8 moveId, qint8 killId, qint8 rowTo, qint8 colTo);           // 保存路径
private:
    bool _canMove(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol, qint8 killId);           // 行走规则
    bool _canMove_JIANG(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                   // 行走规则-将
    bool _canMove_CHE(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                     // 行走规则-车
    bool _canMove_MA(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                      // 行走规则-马
    bool _canMove_XIANG(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                   // 行走规则-象
    bool _canMove_SHI(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                     // 行走规则-仕
    bool _canMove_PAO(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol, qint8 killId);       // 行走规则-炮
    bool _canMove_BING(ChessPiece *_s, qint8 &id, qint8 destRow, qint8 destCol);                    // 行走规则-兵
    bool _againstJiang(ChessPiece *_s, int id, int destCol);                                        // 不能对将
    bool _moveToAgainst(ChessPiece *_s, qint8 id, int destRow, int destCol);                        // 其他棋子走棋之后是否产生对将

private:
    bool &_isRedTurn;                   // 当前是否为红色一方
    qint8 boradTable[10][9];            // 定义棋盘坐标
};

#endif // CONTROL_H
