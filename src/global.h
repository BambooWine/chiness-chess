﻿#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>

// 定义走一步的参数
typedef struct MyStruct {
    qint8 _moveId;
    qint8 _killId;
    qint8 _rowFrom;
    qint8 _colFrom;
    qint8 _rowTo;
    qint8 _colTo;
    MyStruct(): _moveId(0), _killId(0), _rowFrom(0), _colFrom(0), _rowTo(0), _colTo(0) {}
} Step;

#endif // GLOBAL_H
